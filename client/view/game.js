const Matter = require('matter-js');
const GSON = require('gson');

// Matter.js module aliases
const Engine = Matter.Engine;
const World = Matter.World;
const Bodies = Matter.Bodies;
const Render = Matter.Render;

const logPositions = () => {
};

const replaceWorld = world => {
  const newWorld = GSON.parse(world);
  engine.world = newWorld;

  // draw();
};

const update = (error, result) => {
  if (error) {
    throw new Error(error);
  } else {
    replaceWorld(result);
  }
};

const poll = () => {
  Meteor.call('getWorld', update);
};

const draw = () => {
  Render.world(engine);
};

const engine = Engine.create();

Template.game.onRendered(() => {
  Tracker.afterFlush(() => {
    engine.render = Render.create({ element: document.getElementById('viewport') });

    // poll();

    setInterval(poll, 30);
    setInterval(draw, 30);
  });
});

Template.game.events({
  'click .add-boxes-button': (event, template) => {
    Meteor.call('addBoxes', (error, result) => {
      if (error) {
        throw new Meteor.error(error);
      } else {
        poll();
      }
    });
  },
});
