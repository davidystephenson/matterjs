const Matter = require('matter-js');
const GSON = require('gson');

// Matter.js module aliases
const Engine = Matter.Engine;
const World = Matter.World;
const Bodies = Matter.Bodies;
const Render = Matter.Render;

const addBodies = (...bodies) => {
  bodies.map(body => { World.add(engine.world, body); });
};

const addBoxes = () => {
  const boxA = Bodies.rectangle(400, 200, 80, 80);
  const boxB = Bodies.rectangle(450, 50, 80, 80);

  addBodies(boxA, boxB);
};

const setupWorld = () => {
  const ground = Bodies.rectangle(400, 610, 810, 60, { isStatic: true });
  addBodies(ground);

  addBoxes();
};

const updateEngine = () => { Engine.update(engine, 10); };

Meteor.methods({
  getWorld: function() {
    return GSON.stringify(engine.world);
  },

  addBoxes() {
    addBoxes();
  },
});

// Create the physics engine
const engine = Engine.create();

setupWorld();

setInterval(updateEngine, 30);
